package main

import (
	"Test/calculator"
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	fmt.Println("Калькулятор готов к работе")
	scanner := bufio.NewScanner(os.Stdin)

	for {
		fmt.Println("Впишите арифметическую операцию... (для завершения работы калькулятора впишите слово end)")
		scanner.Scan()
		inputData := scanner.Text()
		if strings.ToLower(inputData) == "end" {
			break
		}
		result, err := calculator.Process(inputData)
		if err != nil {
			fmt.Println("Ошибка!", err)
			break
		} else {
			fmt.Println("РЕЗУЛЬТАТ:", result)
		}
	}
	fmt.Println("Калькулятор закончил работу")
}
