package converter

import (
	"sort"
	"strings"
)

// RomanToArabic Римские цифры в арабские
func RomanToArabic(value string) (result int) {
	comparison := map[string]int{
		"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000,
	}
	value = strings.ToUpper(value)

	for k := range value {
		if k < len(value)-1 && comparison[value[k:k+1]] < comparison[value[k+1:k+2]] {
			result -= comparison[value[k:k+1]]
		} else {
			result += comparison[value[k:k+1]]
		}
	}
	return
}

// ArabicToRoman Арабские цифры в римские
func ArabicToRoman(value int) (result string) {
	comparison := map[int]string{
		1000: "M", 900: "CM", 500: "D", 400: "CD", 100: "C", 90: "XC", 50: "L", 40: "XL", 10: "X", 9: "IX", 5: "V", 4: "IV", 1: "I",
	}

	keys := make([]int, 0, len(comparison))
	for k := range comparison {
		keys = append(keys, k)
	}
	sort.Sort(sort.Reverse(sort.IntSlice(keys)))

	for _, k := range keys {
		for value >= k {
			result += comparison[k]
			value -= k
		}
	}
	return
}
