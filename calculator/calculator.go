package calculator

import (
	"Test/converter"
	"errors"
	"log"
	"regexp"
	"strconv"
	"strings"
)

func Process(inputData string) (string, error) {
	// Оператор арифметической операции
	operator, operatorError := getOperator(inputData)
	if operatorError != nil {
		return "", operatorError
	}
	// Операнды арифметической операции
	a, b, roman, operandError := getOperands(strings.ReplaceAll(inputData, " ", ""), operator)
	if operandError != nil {
		return "", operandError
	}
	// Арифметическая операция
	result, calcError := calc(a, b, operator, roman)
	if calcError != nil {
		return "", calcError
	}
	return result, nil
}

// Оператор арифметической операции
func getOperator(inputData string) (operator string, error error) {
	re, err := regexp.Compile(`[\+\-\*\/]`)
	if err != nil {
		log.Fatal(err)
	}
	operatorCollection := re.FindAllString(inputData, -1)
	operatorCount := len(operatorCollection)
	if operatorCount < 1 {
		error = errors.New("введенные данные не являются арифметической операцией")
	} else if operatorCount > 1 {
		error = errors.New("допускается только одна арифметическая операция")
	} else {
		operator = operatorCollection[0]
	}
	return
}

// Операнды арифметической операции
func getOperands(inputData, operator string) (a int, b int, roman bool, error error) {
	re, err := regexp.Compile(`^((\d+)\` + operator + `(\d+))$|^(([ivxIVX]+)\` + operator + `([ivxIVX]+))$`)
	if err != nil {
		log.Fatal(err)
	}
	groups := re.FindStringSubmatch(inputData)
	if len(groups) == 0 {
		error = errors.New("должны использоваться или только целые арабские числа, или только римские числа от 1 до 10")
	} else {
		if groups[1] != "" { // Введены арабские цифры
			a, _ = strconv.Atoi(groups[2])
			b, _ = strconv.Atoi(groups[3])
		} else { // Введены римские цифры
			a = converter.RomanToArabic(groups[5])
			b = converter.RomanToArabic(groups[6])
			roman = true
		}
		if a < 1 || a > 10 || b < 1 || b > 10 {
			error = errors.New("должны использоваться числа от 1 до 10 включительно")
		}
	}
	return
}

// Арифметическая операция
func calc(a, b int, operator string, roman bool) (result string, err error) {
	calcResult := 0
	switch operator {
	case "+":
		calcResult = a + b
	case "-":
		calcResult = a - b
	case "*":
		calcResult = a * b
	case "/":
		calcResult = a / b
	}

	if roman == false { // Арабские цифры
		result = strconv.Itoa(calcResult)
	} else if calcResult < 1 { // Римские отрицательные или 0
		err = errors.New("в римской системе нет нуля и отрицательных чисел")
	} else { // Римские цифры
		result = converter.ArabicToRoman(calcResult)
	}
	return
}
